package delahayeyourself.accountsmanager;

import delahayeyourself.accountsmanager.models.Account;
import delahayeyourself.accountsmanager.utils.Database;
import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author sam
 */
public class Main {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        String dbFilePath;
        String greetings = String.format("Please enter path to database (current path is %s)", System.getProperty("user.dir"));
        System.out.println(greetings);
        dbFilePath = scanner.next();
        Database.setDbPath(dbFilePath);
        List<Account> accounts;
        Account account;
        
        try {
            accounts = Account.all();
            for(Account acc : accounts){
                System.out.println(acc);
            }
            account = Account.one(20);
            System.out.println(account);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
