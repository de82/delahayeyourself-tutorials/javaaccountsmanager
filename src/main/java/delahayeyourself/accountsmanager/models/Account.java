package delahayeyourself.accountsmanager.models;

import delahayeyourself.accountsmanager.utils.Database;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author sam
 */
public class Account {

    protected int id;
    protected String name;
    protected String first_name;
    protected Date birthday;
    protected String iban;
    protected String credit_card_number;
    protected String credit_card_provider;
    protected String credit_card_expire;
    protected int balance;
    
    
    public static List<Account> all() throws ClassNotFoundException, SQLException{
        Connection connection = Database.getConnection();
        Statement statement = Database.createStatement(connection);
        String query = "SELECT * FROM accounts";
        ResultSet resultSet = Database.executeQuery(statement, query);
        List<Account> accounts = new ArrayList<>();
        while(resultSet.next()){
            accounts.add(Account.createFromResultSet(resultSet));
        }
        resultSet.close();
        statement.close();
        connection.close();
        return accounts;
    }
    
    public static Account one(int id) throws SQLException, ClassNotFoundException{
        Account account = null;
        Connection connection = Database.getConnection();
        String query = "SELECT * FROM accounts where id = (?)";
        PreparedStatement preparedStatement = Database.createPreparedStatement(connection, query);
        preparedStatement.setInt(1, id);
        ResultSet resultSet = Database.executePreparedQuery(preparedStatement);
        if(resultSet.next() != false){
            account = Account.createFromResultSet(resultSet);
        }
        resultSet.close();
        preparedStatement.close();
        connection.close();
        return account;
    }
    
    public static Account createFromResultSet(ResultSet resultSet) throws SQLException{
        return new Account(resultSet.getInt("id"), 
                resultSet.getString("name"), 
                resultSet.getString("first_name"), 
                resultSet.getDate("birthday"),
                resultSet.getString("iban"), 
                resultSet.getString("credit_card_number"),
                resultSet.getString("credit_card_provider"),
                resultSet.getString("credit_card_expire"),
                resultSet.getInt("balance")
        );
    }

    public Account(int id, String name, String first_name, Date birthday, String iban, String credit_card_number, String credit_card_provider, String credit_card_expire, int balance) {
        this.id = id;
        this.name = name;
        this.first_name = first_name;
        this.birthday = birthday;
        this.iban = iban;
        this.credit_card_number = credit_card_number;
        this.credit_card_provider = credit_card_provider;
        this.credit_card_expire = credit_card_expire;
        this.balance = balance;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getIban() {
        return iban;
    }

    public void setIban(String iban) {
        this.iban = iban;
    }

    public String getCredit_card_number() {
        return credit_card_number;
    }

    public void setCredit_card_number(String credit_card_number) {
        this.credit_card_number = credit_card_number;
    }

    public String getCredit_card_provider() {
        return credit_card_provider;
    }

    public void setCredit_card_provider(String credit_card_provider) {
        this.credit_card_provider = credit_card_provider;
    }

    public String getCredit_card_expire() {
        return credit_card_expire;
    }

    public void setCredit_card_expire(String credit_card_expire) {
        this.credit_card_expire = credit_card_expire;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    @Override
    public String toString() {
        String information = "Account{id=%s, name=%s, first_name=%s, birthday=%s, iban=%s, credit_card=%s, balance=%s }";
        return String.format(information, this.id, this.name, this.first_name, this.birthday, this.iban, this.getCreditCardInformation(), this.balance);
    }

    public String getCreditCardInformation() {
        return String.format("%s - %s - %s", this.credit_card_number, this.credit_card_provider, this.credit_card_expire);
    }

}
